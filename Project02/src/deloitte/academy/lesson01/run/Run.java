package deloitte.academy.lesson01.run;

import java.util.ArrayList;

import deloitte.academy.lesson01.array.ArrayString;
import deloitte.academy.lesson01.array.ArregloEnteros;
import deloitte.academy.lesson01.array.BooleanArray;
import deloitte.academy.lesson01.array.Curso;

/**
 * Main que se encarga de correr los metodos de: Arregloenteros,
 * 
 * @author nnavarrete
 *
 */
public class Run {

	public static void main(String[] args) {

		// Numero mayor del arreglo
		ArregloEnteros array1 = new ArregloEnteros();
		array1.mayorNum();

		// Numero menor del arreglo
		ArregloEnteros array2 = new ArregloEnteros();
		array2.menor();

		// numeros repetidos
		ArregloEnteros array7 = new ArregloEnteros();
		array7.numerosRepetidos();

		// separador de booleanos
		BooleanArray array3 = new BooleanArray();
		array3.arrayBoolean();

		// Palabra clave

		String cadena = "uno dos tres";
		String palabraClave = "dos";
		
		ArrayString ars = new ArrayString();
		ars.buscaCadena(cadena, palabraClave);
		
		// Registro de datos en el arreglo
		Curso reg1 = new Curso();
		reg1.setNombre("paco");
		reg1.setFecha(181111);

		Curso reg2 = new Curso();
		reg2.setNombre("ana");
		reg2.setFecha(212121);

		Curso reg3 = new Curso();
		reg3.setNombre("brenda");
		reg3.setFecha(121314);

		ArrayList<Curso> cursoRegistro = new ArrayList<Curso>();
		cursoRegistro.add(reg1);
		cursoRegistro.add(reg2);
		
		//Agrega registro
		ArrayList<Curso> objetoResul = new ArrayList<Curso>();
		objetoResul = Curso.addPersona(cursoRegistro, reg3);
		
		//Eliminar registro
		objetoResul = Curso.removePersona(cursoRegistro, reg1);

		//Ordenar registro
		Curso order1 = new Curso();
		order1.Personas();

	}
}
