package deloitte.academy.lesson01.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * { Clase que se encarga de hacer un registro del nombre y fecha,
 * modificaciones y eleminacion.
 * 
 * @author nnavarrete
 *
 */
public class Curso {

	private static final Logger LOGGER = Logger.getLogger(Curso.class.getName());

	public String nombre;
	public Date fecha;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Curso() {
		// TODO Auto-generated constructor stub
	}

	public Curso(String string, Date date) {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Metodo que se encarga de realizar un registro dentro del arreglo.
	 * 
	 * @param cursos
	 * @param curso
	 * @return
	 */
	public static ArrayList<Curso> addPersona(ArrayList<Curso> cursos, Curso curso) {
		ArrayList<Curso> claseCursos = new ArrayList<Curso>();

		claseCursos = cursos;
		claseCursos.add(curso);

		return claseCursos;
	}

	/**
	 * Metodo para eliminar un registro del arreglo
	 * 
	 * @param cursos
	 * @param curso
	 * @return
	 */
	public static ArrayList<Curso> removePersona(ArrayList<Curso> cursos, Curso curso) {
		ArrayList<Curso> claseCursos = new ArrayList<Curso>();

		claseCursos = cursos;

		for (Curso reg : cursos) {

			if (reg.equals(curso)) { // reg.equals.getNombre().curso
				claseCursos.remove(curso);
				break;
			}
		}
		
		

		return claseCursos;
	}

	public void setFecha(int i) {
		// TODO Auto-generated method stub

	}

	/**
	 * metodo que se encarga de ordenar los elementos del arreglo por su fecha
	 */
	public void Personas() {
		List<Curso> cursitos = new ArrayList<Curso>();
		cursitos.add(new Curso("luis", new Date(160921)));
		cursitos.add(new Curso("ana", new Date(180921)));
		cursitos.add(new Curso("maria", new Date(120921)));

		Collections.sort(cursitos, new Comparator<Curso>() {
			@Override
			public int compare(Curso c1, Curso c2) {
				return c1.getFecha().compareTo(c2.getFecha());
			}
		});
		for (Curso curso : cursitos) {
			LOGGER.info("Arrayorder by date: " + curso.getNombre());
		}
	}

	/*public void setFecha(String string) {
		// TODO Auto-generated method stub
		
	}*/
}
