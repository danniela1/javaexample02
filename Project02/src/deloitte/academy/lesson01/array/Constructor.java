package deloitte.academy.lesson01.array;

public class Constructor {
	
	private int id;
	private String nombre;
	

	public Constructor() {
		this.id = 1;
		this.nombre = "daniela";
	}
	
	//constructor using fields 
	public Constructor(int id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public Constructor(int nuevoValor) {
		this();
		id = getId() + nuevoValor;
	}

	//gets & sets
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	

}
