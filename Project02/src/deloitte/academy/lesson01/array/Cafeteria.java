package deloitte.academy.lesson01.array;

public abstract class Cafeteria {
	
	private int idVenta;
	private double importe;
	
	public Cafeteria() {
		// TODO Auto-generated constructor stub
	}

	
	public Cafeteria(int idVenta, double importe) {
		super();
		this.idVenta = idVenta;
		this.importe = importe;
	}


	//gets & sets
	public int getIdVenta() {
		return idVenta;
	}

	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}
	

	public abstract double cobrar();
	

}
