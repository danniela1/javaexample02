package deloitte.academy.lesson01.array;
import java.util.logging.Logger;

/**
 * Esta clase contiene un metodo, el cual se encarga de recibir una cadena 
 * de caracteres y buscara una palabra clave dentro de ella.
 * @author nnavarrete
 *
 */
public class ArrayString {
	
	private static final Logger LOGGER = Logger.getLogger(ArrayString.class.getName());
	
	/**
	 * Metodo que busca una palabra clave dentro de una cadena.
	 */	
	public String buscaCadena(String cadena, String palabraClave)
	{
		
		if( cadena.contains(palabraClave) ) {
			LOGGER.info("palabra encontrada");
		}else {
			LOGGER.info("palabra no encontrada");
		}				
		return palabraClave;
		
	}


}
