package deloitte.academy.lesson01.array;

import java.util.ArrayList;
import java.util.List;

public class EjecutarCafeteria {

	public static final List<Trabajador> listatrabajdor = new ArrayList<Trabajador>();
	public static final int totalRegistros = 0;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Clientes c1 = new Clientes(1,300);
	//	c1.setIdVenta(1); 
	//	c1.setImporte(300);
	//	System.out.println("total a pagar del cliente: " + c1.cobrar());
		
		// // /////////
		Trabajador t1 = new Trabajador(2,300);
	//	t1.setIdVenta(2);
	//	t1.setImporte(300);
	//	System.out.println("total a pagar del empleado: " + t1.cobrar());
		
		ejecutarPago(c1);
		ejecutarPago(t1);
	}
	
	public static void ejecutarPago(Cafeteria cafeteria) {
		System.out.println("total a pagar: " + cafeteria.cobrar());
	}

}
