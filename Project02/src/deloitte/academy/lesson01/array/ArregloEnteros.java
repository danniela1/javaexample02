package deloitte.academy.lesson01.array;

import java.util.ArrayList;

import java.util.logging.Logger;

/**
 * Esta clase se encargara de recibir un arreglo de numeros enteros y
 * dependiendo del metodo seleccionado, regresará el de mayor rango, menor rango
 * o si hay valores repetidos.
 * 
 * @author nnavarrete
 *
 */
public class ArregloEnteros {

	private static final Logger LOGGER = Logger.getLogger(ArregloEnteros.class.getName());

	/**
	 * Metodo que muestra el numero de menor valor dentro de un arreglo de enteros
	 */
	public void menor() {
		// TODO Auto-generated method stub

		int marks[] = { 0, 1, 2, 3, 4, 5, 6, 7 };
		int min = 0;
		// int

		// for each loop
		for (int num : marks) {
			if (num < min) {
				min = num;
			}

		}
		LOGGER.info("Lower Value: " + min);
	}

	/**
	 * Metodo que muestra el numero de mayor valor dentro de un arreglo de enteros
	 */
	public void mayorNum() {

		int max = 0;
		int marks[] = { 12, 43, 655, 43 };

		for (int num : marks) {
			if (num > max) {
				max = num;
			}

		}
		LOGGER.info("Higher value: " + max);

	}

	/**
	 * Metodo que recibe un arreglo de enteros y te regresa un arreglo pero solo los
	 * valores que estan repetidos dentro del arreglo.
	 */
	public void numerosRepetidos() {
		int marks[] = { 4, 4, 6, 3, 4, 5, 8, 7, 4, 1 };
		ArrayList<Integer> valRepetidos = new ArrayList<Integer>();

		for (int p = 0; p < 10; p++) {

			int num = marks[0];

			// for()
			if (num == marks[p]) {
				valRepetidos.add(num);
			}
		}
		LOGGER.info("Valores repetidos: " + valRepetidos);

	}

}
