package deloitte.academy.lesson01.array;

public class EjecutarConstructor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Constructor c1 = new Constructor();
		System.out.println(c1.getNombre() + " " + c1.getId());
		
		Constructor c2 = new Constructor(1, "paco");
		System.out.println(c2.getNombre() + " " + c2.getId());
		
		
		Constructor c3 = new Constructor(100);
		System.out.println("suma: "+  c3.getId());
	}

}
