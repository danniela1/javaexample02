package deloitte.academy.lesson01.array;

import java.util.ArrayList;

import java.util.logging.Logger;

/**
 * Esta clase cuenta con un metodo que recibe una cadena de boolean y los divide
 * en dos listas (true & false).
 * 
 * @author nnavarrete
 *
 */
public class BooleanArray {

	private static final Logger LOGGER = Logger.getLogger(BooleanArray.class.getName());

	/**
	 * Este metodo recibe un arreglo de tipo booleano y dependiendo del contenido
	 * los serpara en dos listas: True & False.
	 */
	

	public void arrayBoolean() {

		ArrayList<Boolean> arrl2 = new ArrayList<Boolean>();
		arrl2.add(false);
		arrl2.add(false);
		arrl2.add(true);
		arrl2.add(true);
		arrl2.add(false);

		ArrayList<Boolean> truelist = new ArrayList<Boolean>();
		ArrayList<Boolean> falselist = new ArrayList<Boolean>();

		arrl2.forEach(n -> {
			if (n == false) {
				falselist.add(n);
			}

			else {
				truelist.add(n);
			}
		});
		LOGGER.info("False List: " + falselist);
		LOGGER.info("False List: " + truelist);

	}

}
